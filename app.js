const { 
    calculateDistance, 
    graphToArray, 
    tripsBetweenTownsWithStops, 
    tripsBetweenTownsRespon,
    getShortRouteResponA,  
    getShortRouteResponB,
    getDifferentRoutesRespon
} = require('./Utils/tourRoute')

// STEP 1 Set Variable
const towns = ['A', 'B', 'C', 'D', 'E'];
const graph = 'AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7';
var response = '';

var { distances = []   , graphDistances = [] } = graphToArray(graph,towns);



//Output 1-5
var consultas = ['ABC', 'AD', 'ADC', 'AEBCD', 'AED'];
consultas.forEach((e, i) => {
    response += `Output #${i+1}: ${calculateDistance(e,distances)} \n`;
});


//Output 6
const tripCount = tripsBetweenTownsRespon("C", "C", 3, graphDistances);
// tripsBetweenTowns("C", "C", 3);
response += `Output #6: ${tripCount} \n`;


//Output 7
var tripCountStops = tripsBetweenTownsWithStops("A", "C", 4, graphDistances);
response += `Output #7: ${tripCountStops} \n`;


//Output 8
var { bestRouteA, bestDistanceA} = getShortRouteResponA("C", "A", 0, graphDistances);
response += `Output #8: ${bestDistanceA} (${bestRouteA}) \n`;


//Output 9
var { bestRouteB, bestDistanceB} = getShortRouteResponB("B", "B", 0, graphDistances);
response += `Output #9: ${bestDistanceB} (${bestRouteB}) \n`;


//Output 10
var differentRoutesCount = getDifferentRoutesRespon("C", "C", 0, graphDistances);
response += `Output #10: ${differentRoutesCount} \n`;


console.log(response)
