var tripCount = 0;
var bestRouteA = "";
var bestDistanceA = Infinity + 1;
var bestRouteB = "";
var bestDistanceB = Infinity + 1;
var differentRoutesCount = 0;


// STEP 2 
// set graphDistances and distances frome  graph and towns 
const graphToArray = (graph ,towns ) => {
    console.log('graph =',graph)
    console.log('towns =',towns)
    console.log('')
    var graphToArray = graph.replace(/ /g, '').split(',');
    var graphDistances = [];
    var distances = [];
    graphToArray.forEach(item => {
        distances.push({ origin: item[0], destination: item[1], distance: parseInt(item[2]) });
    });
    
    console.warn('graphDistances')
    towns.forEach(element => {
        console.log("%cTown" ,'color: white; background-color: blue; margin:5px;',element)
        console.log(distances.filter(e => e.origin === element))
        graphDistances.push({
            town: element,
            destinations: distances.filter(e => e.origin === element)
        });
    });
    
    console.log('')
    console.log('distances')
    console.log(distances)
    console.log('')
    return { graphDistances , distances }
}


// Step 3 ใช้คิด Test case 1 - 5

// Calculate distance of the route (test case 1-5)
const calculateDistance = (route, distances) => {
    
    // example : The distance of the route A-B-C. 
    // a = [ A , B , C ]
    let a = route.replace(/ /g, '').split('');
    
    let totalDistance = 0;
    
    //ตัด ตัวสุดเท้ายออก คิดแค่ระยะทางจาก A ไป B + A ไป C
    a.slice(0, a.length - 1).forEach((i, index) => {
        
         //end คือตัวถัดไป ตัวอย่าง คือ B
        let end = a[index + 1];
        
        // loop แรก คือ A ค้นหา origin = A  destination = B 
        var c = distances.find(d => {
            return (d.origin == i && d.destination == end);
        });
        if (c) {
            // ค่าที่ได้ รอบแรก ของตัวอย่างคือ 
            //  { origin: 'A', destination: 'B', distance: 5 }
            // distance = 5
            
            // ค่าที่ได้รอบที่ 2 คือ 
            // { origin: 'B', destination: 'C', distance: 4 }
             // distance = 4
            totalDistance += c.distance;
        } else {
            totalDistance = 0;
            return;
        }
    });
    // ตัวอย่าง ข้อแรก  The distance of the route A-B-C.  = 5 + 4 = 9
    return (totalDistance != 0) ? totalDistance : 'NO SUCH ROUTE';
}

const getDifferentRoutes = (end, route, distance, graphDistances)  => {
    if (distance >= 30) {
        return;
    }
    if (distance > 0 && route.endsWith(end)) {
        differentRoutesCount++;
    }
    var lastChar = route.charAt(route.length - 1);
    var matrizB = graphDistances.find(e => e.town === lastChar);
    for (var i = 0; i < matrizB.destinations.length; i++) {
        var newChar = matrizB.destinations[i].destination;
        var newCost = matrizB.destinations[i].distance;
        getDifferentRoutes(end, route + newChar, distance + newCost, graphDistances);
    }
}

const tripsBetweenTownsWithStops = (start, end, stops, graphDistances) => {
    var lastRoute = start;
    for (var stop = 0; stop < stops; stop++) {
        var route = "";
        for (var i = 0; i < lastRoute.length; i++) {
            var c = lastRoute.charAt(i);
            var matrizB = graphDistances.find(e => e.town === c);
            for (var j = 0; j < matrizB.destinations.length; j++) {
                route = route + matrizB.destinations[j].destination;
            }
        }
        lastRoute = route;
    }
    return (lastRoute.split(end).length - 1);
}

const getShortRouteA = (end, route, distance, graphDistances) => {
    if (route.endsWith(end) && distance < bestDistanceA && distance > 0) {
        bestRouteA = route;
        bestDistanceA = distance;

    }
        var lastChar = route.charAt(route.length - 1);
        var matrizB = graphDistances.find(e => e.town === lastChar);
        for (var i = 0; i < matrizB.destinations.length; i++) {
            var newChar = matrizB.destinations[i].destination;
            var newCost = matrizB.destinations[i].distance;
            if (route.indexOf(newChar) > 0)
                continue;
                getShortRouteA(end, route + newChar, distance + newCost, graphDistances);
        
    }

}

const getShortRouteB = (end, route, distance, graphDistances) => {
    if (route.endsWith(end) && distance < bestDistanceB && distance > 0) {
        bestRouteB = route;
        bestDistanceB = distance;

    }
        var lastChar = route.charAt(route.length - 1);
        var matrizB = graphDistances.find(e => e.town === lastChar);
        for (var i = 0; i < matrizB.destinations.length; i++) {
            var newChar = matrizB.destinations[i].destination;
            var newCost = matrizB.destinations[i].distance;
            if (route.indexOf(newChar) > 0)
                continue;
                getShortRouteB(end, route + newChar, distance + newCost, graphDistances);
        
    }

}

const tripsBetweenTowns = (end, route, maxStops, graphDistances) => {
    if (route.length - 1 > maxStops) return;
    if (route.length > 1 && route.endsWith(end)) {
        tripCount++;
    }
    var lastChar = route.charAt(route.length - 1);
    var matrizB = graphDistances.find(e => e.town === lastChar);
    for (var i = 0; i < matrizB.destinations.length; i++) {
        var newChar = matrizB.destinations[i].destination;
        tripsBetweenTowns(end, route + newChar, maxStops,graphDistances);
    }
    
}

const tripsBetweenTownsRespon = (end, route, maxStops, graphDistances) => {
    tripsBetweenTowns(end, route, maxStops, graphDistances)
    return tripCount
}

const getShortRouteResponA =  (end, route, distance, graphDistances)  => {
    getShortRouteA(end, route, distance, graphDistances)
    return {
        bestRouteA,
        bestDistanceA
    }
}

const getShortRouteResponB =  (end, route, distance, graphDistances)  => {
    getShortRouteB(end, route, distance, graphDistances)
    return {
        bestRouteB,
        bestDistanceB
    }
}

const getDifferentRoutesRespon =  (end, route, distance, graphDistances)  => {
    getDifferentRoutes(end, route, distance, graphDistances)
    return differentRoutesCount
}

module.exports = {
    calculateDistance, 
    graphToArray, 
    tripsBetweenTownsRespon,
    tripsBetweenTownsWithStops,
    getShortRouteResponA,
    getShortRouteResponB,
    getDifferentRoutesRespon
}
