# Problem: A Tour Route Planner

## Background
The local commuter railroad services a number of towns in Kiwiland. Because of monetary concerns, all of the tracks are 'one-way.' That is, a route from Kaitaia to Invercargill does not imply the existence of a route from Invercargill to Kaitaia. In fact, even if both of these routes do happen to exist, they are distinct and are not necessarily the same distance!

The purpose of this problem is to help the railroad provide its customers with information about the routes. In particular, you will compute the distance along a certain route, the number of different routes between two towns, and the shortest route between two towns.

## Input:
A directed graph where a node represents a town and an edge represents a route between two towns. The weighting of the edge represents the distance between the two towns. A given route will never appear more than once, and for a given route, the starting and ending town will not be the same town.

## Output:
For test input 1 through 5, if no such route exists, output 'NO SUCH ROUTE'. Otherwise, follow the route as given; do not make any extra stops! For example, the first problem means to start at city A, then travel directly to city B (a distance of 5), then directly to city C  (a distance of 4).

    The distance of the route A-B-C.
    The distance of the route A-D.
    The distance of the route A-D-C.
    The distance of the route A-E-B-C-D.
    The distance of the route A-E-D.
    The number of trips starting at C and ending at C with a maximum of 3 stops. In the sample data below, there are two such trips: C-D-C (2 stops). and C-E-B-C (3 stops).
    The number of trips starting at A and ending at C with exactly 4 stops. In the sample data below, there are three such trips: A to C (via B,C,D); A to C (via D,C,D); and A to C (via D,E,B).
    The length of the shortest route (in terms of distance to travel) from A to C.
    The length of the shortest route (in terms of distance to travel) from B to B.
    The number of different routes from C to C with a distance of less than 30. In the sample data, the trips are:

## Test Input:

For the test input, the towns are named using the first few letters of the alphabet from A to D. A route between two towns (A to B) with a distance of 5 is represented as AB5.

* Graph: AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7

Expected Output:

    - Output 1: 9
    - Output 2: 5
    - Output 3: 13
    - Output 4: 22
    - Output 5: NO SUCH ROUTE
    - Output 6: 2
    - Output 7: 3
    - Output 8: 9
    - Output 9: 9
    - Output 10: 7

<!-- blank line -->
----
<!-- blank line -->

## Install 

```
npm install
```
```
npm test
```
```
npm start
```



<!-- blank line -->
----
<!-- blank line -->

### STEP 1 Set Variable
**Note:** จากโจทที่ให้มา คือ graph และ towns.

```javaScript
graph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
towns = [ 'A', 'B', 'C', 'D', 'E' ]
 ```
 
### STEP 2 GraphDistances
**Note:** นำ graph และ towns มาสร้างเป็น graph ที่แสดง node ต่างๆ แทนด้วยเมือง และ ระยะทางระหว่างเมือง(distance) เช่น 
AB5 คือ ระยะทางจากเมือง A ไป เมือง B มีค่าเท่ากับ 5 .

![Semantic description of image](/src/graph.png "Graph Distances")

#### Graph Distances Structure

```javaScript
Town A
[
  { origin: 'A', destination: 'B', distance: 5 },
  { origin: 'A', destination: 'D', distance: 5 },
  { origin: 'A', destination: 'E', distance: 7 }
]

Town B
[ { origin: 'B', destination: 'C', distance: 4 } ]

Town C
[
  { origin: 'C', destination: 'D', distance: 8 },
  { origin: 'C', destination: 'E', distance: 2 }
]

Town D
[
  { origin: 'D', destination: 'C', distance: 8 },
  { origin: 'D', destination: 'E', distance: 6 }
]

Town E
[ { origin: 'E', destination: 'B', distance: 3 } ]

 ```
#### Distances Structure

```javaScript
[
  { origin: 'A', destination: 'B', distance: 5 },
  { origin: 'B', destination: 'C', distance: 4 },
  { origin: 'C', destination: 'D', distance: 8 },
  { origin: 'D', destination: 'C', distance: 8 },
  { origin: 'D', destination: 'E', distance: 6 },
  { origin: 'A', destination: 'D', distance: 5 },
  { origin: 'C', destination: 'E', distance: 2 },
  { origin: 'E', destination: 'B', distance: 3 },
  { origin: 'A', destination: 'E', distance: 7 }
]
 ```
### Step 3 ใช้คิด Test case 1 - 5
**Note:** คำนวนหาเส้นทาง โดยใช้  function calculateDistance()

#### อธิบายการทำงาน ของ function ดังนี้ 
```javaScript
// Calculate distance of the route (test case 1-5)
const calculateDistance = (route, distances) => {
    
    // example : The distance of the route A-B-C. 
    // a = [ A , B , C ]
    let a = route.replace(/ /g, '').split('');
    
    let totalDistance = 0;
    
    //ตัด ตัวสุดเท้ายออก คิดแค่ระยะทางจาก A ไป B + A ไป C
    a.slice(0, a.length - 1).forEach((i, index) => {
        
         //end คือตัวถัดไป ตัวอย่าง คือ B
        let end = a[index + 1];
        
        // loop แรก คือ A ค้นหา origin = A  destination = B 
        var c = distances.find(d => {
            return (d.origin == i && d.destination == end);
        });
        if (c) {
            // ค่าที่ได้ รอบแรก ของตัวอย่างคือ 
            //  { origin: 'A', destination: 'B', distance: 5 }
            // distance = 5
            
            // ค่าที่ได้รอบที่ 2 คือ 
            // { origin: 'B', destination: 'C', distance: 4 }
             // distance = 4
            totalDistance += c.distance;
        } else {
            totalDistance = 0;
            return;
        }
    });
    // ตัวอย่าง ข้อแรก  The distance of the route A-B-C.  = 5 + 4 = 9
    return (totalDistance != 0) ? totalDistance : 'NO SUCH ROUTE';
    // ถ้า totalDistance == 0 หมายถึง find ไม่เจอเส้นทาง ให้ขึ้น NO SUCH ROUTE
}
 ```
