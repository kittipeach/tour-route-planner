const { 
    calculateDistance, 
    graphToArray, 
    tripsBetweenTownsWithStops, 
    tripsBetweenTownsRespon,
    getShortRouteResponA,  
    getShortRouteResponB,
    getDifferentRoutesRespon
} = require('./Utils/tourRoute')

const towns = ['A', 'B', 'C', 'D', 'E'];
const graph = 'AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7';

var { distances = []   , graphDistances = [] } = graphToArray(graph,towns);

describe('test', () => {
      test('The distance of the route A-B-C.', () => {
        expect(calculateDistance('ABC',distances)).toBe(9);
      });
      test('The distance of the route A-D.', () => {
        expect(calculateDistance('AD',distances)).toBe(5);
      });
      test('The distance of the route A-D-C.', () => {
        expect(calculateDistance('ADC',distances)).toBe(13);
      });
      test('The distance of the route A-E-B-C-D.', () => {
        expect(calculateDistance('AEBCD',distances)).toBe(22);
      });
      test('The distance of the route A-E-D.', () => {
        expect(calculateDistance('AED',distances)).toBe('NO SUCH ROUTE');
      });
      
      test('The number of trips starting at C and ending at C with a maximum of 3 stops. In the sample data below, there are two such trips: C-D-C (2 stops). and C-E-B-C (3 stops).', () => {
       expect(tripsBetweenTownsRespon("C", "C", 3, graphDistances)).toBe(2);
     });
     
     test('The number of trips starting at A and ending at C with exactly 4 stops. In the sample data below, there are three such trips: A to C (via B,C,D); A to C (via D,C,D); and A to C (via D,E,B).', () => {
        expect(tripsBetweenTownsWithStops("A", "C", 4, graphDistances)).toBe(3);
      });
      
    test('The length of the shortest route (in terms of distance to travel) from A to C.', () => {
        expect(getShortRouteResponA("C", "A", 0, graphDistances).bestDistanceA).toBe(9);
    });
    
    test('The length of the shortest route (in terms of distance to travel) from B to B.', () => {
        expect(getShortRouteResponB("B", "B", 0, graphDistances).bestDistanceB).toBe(9);
    });
    
    test('The number of different routes from C to C with a distance of less than 30. In the sample data, the trips are: CDC, CEBC, CEBCDC, CDCEBC, CDEBC, CEBCEBC, CEBCEBCEBC.', () => {
        expect( getDifferentRoutesRespon("C", "C", 0, graphDistances)).toBe(7);
    });

});
